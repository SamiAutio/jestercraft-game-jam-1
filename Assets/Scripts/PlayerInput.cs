﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum InputType{
	Move, Hit
}

public class PlayerInput : MonoBehaviour {
	public AudioClip hitSound;
	public AudioClip powerupSound;
	public LayerMask layerMask;
	public GameObject staminaBarPrefab;
	public List<GameObject> staminaBarImages;
	public Transform staminaImageHolder;
	public GameObject gameOverScreen;
	public GameObject winScreen;
	public int stamina;
	public int maxStamina;
	public float benchStaminaRecoveryspeed;
	private Rigidbody2D rb2D;
	private bool moving = false;
	private bool hitting = false;
	public bool win = false;
	private Vector3 checkStartPosition;
	private Vector3 checkPosition;
	private Animator animator;
	private bool facingRight = true;
	private bool nearBench = false;
	private bool onBench = false;
	private AudioSource audioSource;
	
	private Vector3 benchPosition;
	private IEnumerator sit;
	
	// Use this for initialization
	void Start ()
	{
		rb2D = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
		sit = Sit();

		animator.SetInteger("Stamina", stamina);

		for(int i=0; i < maxStamina; i++){
			GameObject go = Instantiate(staminaBarPrefab);
			go.transform.SetParent(staminaImageHolder, false);
			
			if(stamina > i){
				go.SetActive(true);
			}
			else
			{
				go.SetActive(false);
			}
			staminaBarImages.Add(go);
		}
		UpdateStaminaBars();
	}
	
	// Update is called once per frame
	void Update () 
	{	
		if(win && Input.anyKeyDown || Input.GetKeyDown(KeyCode.Escape)){
			#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
			#endif
			Application.Quit();
		}
		if(!moving && !hitting && stamina > 0){

			if (nearBench && Input.GetButtonDown("Submit")){
				Debug.Log("Bench!");
				sit = Sit();
				onBench = true;
				StartCoroutine(sit);
				animator.SetBool("OnBench", onBench);
				
			} else if (onBench && Input.anyKeyDown){
				onBench = false;
				StopCoroutine(sit);
				animator.SetBool("OnBench", onBench);
			}

			if(!onBench){
				if(Input.GetAxis("Horizontal") > 0)
				{
					TryMoveOrHit(transform.position + Vector3.right, InputType.Move);
				} 
				else if (Input.GetAxis("Horizontal") < 0)
				{
					TryMoveOrHit(transform.position + Vector3.left, InputType.Move);
				} 
				else if (Input.GetAxis("Vertical") < 0)
				{
					TryMoveOrHit(transform.position + Vector3.down, InputType.Hit);
				}
				else if (Input.GetAxis("Vertical") > 0)
				{
					TryMoveOrHit(transform.position + Vector3.up, InputType.Hit);
				}else if(Input.GetButtonDown("Jump")){
					Jump();
				} 
			}
		}
	}

	private void Jump(){
		Vector3 targetPosition = transform.position + Vector3.down;
		MoveTargetAndStartPositions(targetPosition);

		Debug.DrawLine(checkStartPosition, checkPosition, Color.yellow, 0.24f);

		Vector3 targetPosition2 = checkStartPosition + Vector3.up;

		if(Physics2D.Linecast(checkStartPosition, checkPosition, layerMask) && !Physics2D.Linecast(checkStartPosition, targetPosition2, layerMask)){
			rb2D.MovePosition(transform.position + Vector3.up);
		}
	}

	private void TryMoveOrHit(Vector3 vector, InputType i){
		switch(i){
			case InputType.Move:
				StartCoroutine(Move(vector));
				break;
			case InputType.Hit:
				StartCoroutine(Hit(vector));
				break;
			default:
				Debug.Log("Expected enum of type InputType.");
				break;
		}
	}

	IEnumerator Hit(Vector3 targetPosition){
		hitting = true;

		MoveTargetAndStartPositions(targetPosition);
		
		Debug.DrawLine(checkStartPosition, checkPosition, Color.red, 0.25f);

		if(Physics2D.Linecast(checkStartPosition, checkPosition, layerMask) && stamina > 0){
			Debug.Log("HIT!");
			RaycastHit2D hit = Physics2D.Linecast(checkStartPosition, checkPosition, layerMask);

			if(targetPosition == transform.position + Vector3.left)
			{
				animator.SetTrigger("HitLeft");
				checkPosition = checkStartPosition + Vector3.left;
				
			} else if(targetPosition == transform.position + Vector3.right)
			{
				animator.SetTrigger("HitRight");
				checkPosition = checkStartPosition + Vector3.right;
			}
			else if(targetPosition == transform.position + Vector3.down)
			{
				animator.SetTrigger("HitDown");
				checkPosition = checkStartPosition + Vector3.down;
			}
			else if(targetPosition == transform.position + Vector3.up)
			{
				animator.SetTrigger("HitUp");
				checkPosition = checkStartPosition + Vector3.up;
			}

			if(hit.transform.GetComponent<Breakable>() != null)
			{
				hit.transform.GetComponent<Breakable>().Damage(1);
				audioSource.clip = hitSound;
				audioSource.Play();
				SubstractStamina(1);
			}
		}

		yield return new WaitForSeconds(.525f);

		hitting = false;
	}

	IEnumerator Move(Vector3 targetPosition){
		MoveTargetAndStartPositions(targetPosition);

		Debug.DrawLine(checkStartPosition, checkPosition, Color.green, 0.24f);
		
		if(!Physics2D.Linecast(checkStartPosition, checkPosition, layerMask)){
			moving = true;

			rb2D.MovePosition(targetPosition);

			yield return new WaitForSeconds(.25f);

			moving = false;
		} else {
			moving = false;
			StartCoroutine(Hit(targetPosition));
			
		}
	}
	IEnumerator Sit(){
		while (stamina < maxStamina){
			yield return new WaitForSeconds(benchStaminaRecoveryspeed);
			AddStamina(1);
		}
	}

	private void MoveTargetAndStartPositions(Vector3 targetPosition){
		checkStartPosition = transform.position + Vector3.down/2 + Vector3.right /2; 
		
		checkPosition = targetPosition;

		if(targetPosition == transform.position + Vector3.left)
		{
			checkPosition = checkStartPosition + Vector3.left;
			
		} else if(targetPosition == transform.position + Vector3.right)
		{
			checkPosition = checkStartPosition + Vector3.right;
		}
		else if(targetPosition == transform.position + Vector3.down)
		{
			checkPosition = checkStartPosition + Vector3.down;
		}
		else if(targetPosition == transform.position + Vector3.up)
		{
			checkPosition = checkStartPosition + Vector3.up;
		}
	}

	public void AddStamina(int p_stamina){
		stamina += p_stamina;
		if(stamina > maxStamina){
			stamina = maxStamina;
		}
		animator.SetInteger("Stamina", stamina);
		audioSource.clip = powerupSound;
		audioSource.Play();
		UpdateStaminaBars();
	}
	public void SubstractStamina(int p_stamina){
		stamina -= p_stamina;
		if(stamina < 0){
			stamina = 0;
		}

		if(stamina == 0){
			gameOverScreen.SetActive(true);
		}

		UpdateStaminaBars();
		animator.SetInteger("Stamina", stamina);
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Food"){
			AddStamina(other.GetComponent<Food>().staminaRecovery);
			Destroy(other.gameObject);
		}
		if(other.tag == "Bench"){
			nearBench = true;
		}
		if(other.tag == "Finish"){
			win = true;
			winScreen.SetActive(true);
		}
	}
	private void OnTriggerExit2D(Collider2D other) {
		if(other.tag == "Bench"){
			nearBench = false;
		}
	}

	private void UpdateStaminaBars(){
		int i = 0;
		foreach(GameObject go in staminaBarImages){
			if(stamina > i){
				go.SetActive(true);
			}
			else
			{
				go.SetActive(false);
			}
			i++;
		}
	}
}
