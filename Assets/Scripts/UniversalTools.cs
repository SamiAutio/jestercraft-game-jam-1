﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UniversalTools {

	public static Vector3[] GetCenteredPositions(Vector3 startPosition, Vector3 targetPosition){
		Vector3 checkStartPosition = startPosition + Vector3.down/2 + Vector3.right /2; 
		
		Vector3 checkPosition = targetPosition;
		

		if(targetPosition == startPosition + Vector3.left)
		{
			checkPosition = checkStartPosition + Vector3.left;
			
		} else if(targetPosition == startPosition + Vector3.right)
		{
			checkPosition = checkStartPosition + Vector3.right;
		}
		else if(targetPosition == startPosition + Vector3.down)
		{
			checkPosition = checkStartPosition + Vector3.down;
		}
		else if(targetPosition == startPosition + Vector3.up)
		{
			checkPosition = checkStartPosition + Vector3.up;
		}

		Vector3[] vectors = {checkStartPosition, checkPosition};
		
		return vectors;
	}
	public static Vector3 GetCenteredPosition(Vector3 startPosition){
		Vector3 checkStartPosition = startPosition + Vector3.down/2 + Vector3.right /2;
		return checkStartPosition;
	}
}
