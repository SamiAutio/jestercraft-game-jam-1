﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour {

	public int health;

	private Animator animator;	
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
	}
	public void Damage(int damage){
		health -= damage;
		animator.SetInteger("Health", health);
		Debug.Log(name + "'s Health: " +health);
		if(health <= 0){
			Destroy(gameObject, 0.1f);
			
		}
	}
}
