# Game Made for Jestercraft Game Jam #1 in 24 hours

https://sambi.itch.io/mistverse-burrow

**Movement and burrowing:** WASD

**Jump/Interact:** Space


**Theme:** MIX OF THREE

**Mixed features:**

 * Benches from Hollow Knight
 * Digging from SteamWorld Dig
 * Stamina from The Legend of Zelda: Breath of the Wild

Got a bit sidetracked with Unity's Tilemaps and had to ditch some of the work I had done to get something done in the time limit :).